<?php

use yii\db\Schema;
use yii\db\Migration;

class m150708_082910_fill_book_table extends Migration
{       
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $books = [
            [
                'name' => 'Война и мир',
                'preview' => '',
                'date_create' => date('Y-m-d'),
                'date_update' => date('Y-m-d'),
                'date' => '',
                'author_id' => '',
            ],
            [
                'name' => 'Мастер и Маргарита',
                'preview' => '',
                'date_create' => date('Y-m-d'),
                'date_update' => date('Y-m-d'),
                'date' => '',
                'author_id' => '',
            ],
            [
                'name' => 'Преступление и наказание',
                'preview' => '',
                'date_create' => date('Y-m-d'),
                'date_update' => date('Y-m-d'),
                'date' => '',
                'author_id' => '',
            ],
            [
                'name' => 'Братья Карамазовы',
                'preview' => '',
                'date_create' => date('Y-m-d'),
                'date_update' => date('Y-m-d'),
                'date' => '',
                'author_id' => '',
            ],
            [
                'name' => 'Анна Каренина',
                'preview' => '',
                'date_create' => date('Y-m-d'),
                'date_update' => date('Y-m-d'),
                'date' => '',
                'author_id' => '',
            ],
            [
                'name' => 'Мёртвые души',
                'preview' => '',
                'date_create' => date('Y-m-d'),
                'date_update' => date('Y-m-d'),
                'date' => '',
                'author_id' => '',
            ],
            [
                'name' => 'Идиот',
                'preview' => '',
                'date_create' => date('Y-m-d'),
                'date_update' => date('Y-m-d'),
                'date' => '',
                'author_id' => '',
            ],
            [
                'name' => 'Собачье сердце',
                'preview' => '',
                'date_create' => date('Y-m-d'),
                'date_update' => date('Y-m-d'),
                'date' => '',
                'author_id' => '',
            ],
            [
                'name' => 'Бесы',
                'preview' => '',
                'date_create' => date('Y-m-d'),
                'date_update' => date('Y-m-d'),
                'date' => '',
                'author_id' => '',
            ],
            
        ];
        
        foreach($books as $book){
            $this->insert('{{%book}}', [
                'name' => $book['name'],
                'date_create' => $book['date_create'],
                'date_update' => $book['date_update'],
            ]);
        }

    }

    public function down()
    {

    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
