<?php

use yii\db\Schema;
use yii\db\Migration;

class m150708_080759_create_tables extends Migration
{
    public function up()
    {   
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%book}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'date_create' => Schema::TYPE_DATETIME . ' NOT NULL',
            'date_update' => Schema::TYPE_DATETIME . ' DEFAULT NULL',
            'preview' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'date' => Schema::TYPE_DATE . ' DEFAULT NULL',
            'author_id' => Schema::TYPE_INTEGER . ' DEFAULT NULL',
        ], $tableOptions);
        
        $this->createTable('{{%author}}', [
            'id' => Schema::TYPE_PK,
            'firstname' => Schema::TYPE_STRING . ' NOT NULL',
            'lastname' => Schema::TYPE_STRING . ' NOT NULL',
        ], $tableOptions);
        
        $this->addForeignKey("fk_book_author", "{{%book}}", "author_id", "{{%author}}", "id");
    }

    public function down()
    {
        $this->dropTable('{{%book}}');
        $this->dropTable('{{%author}}');
    }
}
