<?php

use yii\db\Schema;
use yii\db\Migration;

class m150708_082904_fill_author_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $authors = [
            [
                'firstname' => 'Михаил',
                'lastname'  => 'Булгаков',
            ],
            [
                'firstname' => 'Лев',
                'lastname'  => 'Толстой',
            ],
            [
                'firstname' => 'Федор',
                'lastname'  => 'Достоевский',
            ],
            [
                'firstname' => 'Николай',
                'lastname'  => 'Гоголь',
            ],
            [
                'firstname' => 'Иван',
                'lastname'  => 'Тургенев',
            ],
            [
                'firstname' => 'Антон',
                'lastname'  => 'Чехов',
            ],
        ];
        
        foreach($authors as $author){
            $this->insert('{{%author}}', [
                'firstname' => $author['firstname'],
                'lastname' => $author['lastname'],
            ]);
        }

    }

    public function down()
    {
        $this->truncateTable('{{%author}}');
    }
}
