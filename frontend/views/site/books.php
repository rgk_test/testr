<?php
//use Yii;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use newerton\fancybox\FancyBox;

/* @var $this yii\web\View */
$this->title = 'My Yii Application';
echo newerton\fancybox\FancyBox::widget([
    'target' => 'a[rel=fancybox]',
    'helpers' => true,
    'mouse' => true,
    'config' => [
        'maxWidth' => '90%',
        'maxHeight' => '90%',
        'playSpeed' => 7000,
        'padding' => 0,
        'fitToView' => false,
        'width' => '70%',
        'height' => '70%',
        'autoSize' => false,
        'closeClick' => false,
        'openEffect' => 'elastic',
        'closeEffect' => 'elastic',
        'prevEffect' => 'elastic',
        'nextEffect' => 'elastic',
        'closeBtn' => false,
        'openOpacity' => true,
        'helpers' => [
            'title' => ['type' => 'float'],
            'buttons' => [],
            'thumbs' => ['width' => 68, 'height' => 50],
            'overlay' => [
                'css' => [
                    'background' => 'rgba(0, 0, 0, 0.8)'
                ]
            ]
        ],
    ]
]);

?>
<div class="site-index">



    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                <?php
                echo GridView::widget([
                    'dataProvider' => $dataProvider,
//                    'filter' => $model,
                    'columns' => [
                        'id',
                        'name',
                        [
                            'attribute' => 'preview',
                            'format' => 'raw',
                            'value' => function($data) {
                                return Html::a(Html::img('../upload/preview/'.$data->preview), '../upload/'.$data->preview, ['rel' => 'fancybox']);
                            },
                        ],
                        [
                            'attribute' => 'date_create',
                            'format' => 'raw',
                            'value' => function($data) { return $data->date_create?\Yii::t('app', '{0, date}', strtotime($data->date_create)):''; },
                        ],
                        [
                            'attribute' => 'date_update',
                            'format' => 'raw',
                            'value' => function($data) { return $data->date_update?\Yii::t('app', '{0, date}', strtotime($data->date_update)):''; },
                        ],
                        [           
                            'attribute' => 'date',
                            'format' => 'raw',
                            'value' => function($data) { return $data->date?\Yii::t('app', '{0, date}', strtotime($data->date)):''; },
                        ],
                                    
                                    
                        'author',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'contentOptions' => ['style' => 'width:260px;'],
                            'header' => 'Actions',
                            'template' => '{view} {edit} {delete}',
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    return Html::a('<span class="fa fa-search"></span>View', FALSE, [
                                                'value' => Url::to(['site/bookview', 'id' => $model->id]),
                                                'title' => 'View book',
                                                'class' => 'btn btn-primary btn-xs showModal',
                                    ]);
                                },
                                'edit' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span> ', FALSE, [
                                                'value' => Url::to(['site/bookedit', 'id' => $model->id]),
                                                'title' => 'Edit book',
                                                'class' => 'btn btn-primary btn-xs book-edit-link showModal',
                                    ]);
                                },
                                'delete' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::to(['site/bookdelete', 'id' => $model->id]), [
                                                'title' => 'Delete book',
                                                'class' => 'book-delete-link',
                                    ]);
                                },
                                ],
                        ],
                            ],
                        ]); 
                        ?>
            </div>
        </div>

    <?php Modal::begin([
    'id' => 'book-modal',
    'header' => '<h4 class="modal-title"></h4>',
    'footer' => '<a " href="#" class="btn btn-primary" data-dismiss="modal">Close</a>',

]); 
    yii\bootstrap\Modal::end();
    ?>
    


</div>
