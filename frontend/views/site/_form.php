<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\models\Author;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use yii\widgets\Pjax;


?>
<?php
Pjax::begin(['id' => 'new_book']);
$form = ActiveForm::begin([
            'id' => 'book-edit-form',
            'options' => ['class' => 'form-horizontal',
            'enctype'=>'multipart/form-data',
                'data-pjax' => true
                ],
        ]);
?>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <?=$form->field($book, 'name')?>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
       <?=$form->field($book,'date')->widget(DatePicker::className(),['clientOptions' => ['format' => 'yyyy MM dd - HH:ii P',]])?>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <?=$form->field($book, 'author_id')->dropDownList(array_merge(['' => 'Select author'],ArrayHelper::map(Author::find()->all(), 'id', 'fullName')))?>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-5">
        <?=Html::img('upload/preview/'.$book->preview)?>
    </div>
    <div class="col-md-5">
        
        <?=$form->field($book, 'preview')->fileInput();?>
    </div>
    <div class="col-md-1"></div>
-    <div class="row">
            <div class="col-md-9"></div>
    <div class="col-md-2">
        
        <?= Html::submitButton($book->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $book->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    
 
    </div>
    <div class="col-md-1"></div>
        
    </div>
</div>

<?php
ActiveForm::end();
        Pjax::end();
        ?>

