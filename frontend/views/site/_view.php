<?php
use yii\helpers\Html;
?>
<div class="row">
    <div class="col-md-12">
        <?=$book->name?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?=$book->author?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?=$book->date_create?\Yii::t('app', '{0, date}', strtotime($book->date_create)):'';?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?=$book->date_create?\Yii::t('app', '{0, date}', strtotime($book->date_create)):'';?>
    </div>
</div><div class="row">
    <div class="col-md-12">
        <?=$book->date_create?\Yii::t('app', '{0, date}', strtotime($book->date)):'';?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?=Html::a(Html::img('../upload/preview/'.$book->preview), '../upload/'.$book->preview, ['rel' => 'fancybox'])?>
    </div>
</div>




