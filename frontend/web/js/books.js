$(document).ready(function(){
        $(document).on('click', '.showModal', function () {
            $('#book-modal').modal('show');
            $('.modal-title').html($(this).attr('title'));
            $('.modal-body').load($(this).attr('value'));
            
    });
     $(document).on('click', '.button-submit-form', function () {
            $('#new_book > form').submit();
    });
});