<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "book".
 *
 * @property integer $id
 * @property string $name
 * @property string $date_create
 * @property string $date_update
 * @property string $preview
 * @property string $date
 * @property integer $author_id
 *
 * @property Author $author
 */
class Book extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'book';
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->date = date('Y-m-d', strtotime(str_replace(",", "", $this->date)));
            return TRUE;
        } else
            return false;
    }

    public function afterFind() {
        parent::afterFind();
        $this->date = date('d F, Y', strtotime(str_replace("-", "", $this->date)));
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'date_create'], 'required'],
            [['date_create', 'date_update', 'date'], 'safe'],
            [['author_id'], 'integer'],
            [['name', 'preview'], 'string', 'max' => 255],
            [['preview'], 'file', 'extensions' => 'jpg, gif, png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'date_create' => 'Date Create',
            'date_update' => 'Date Update',
            'preview' => 'Preview',
            'date' => 'Date',
            'author_id' => 'Author ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor() {
        return $this->hasOne(Author::className(), ['id' => 'author_id']);
    }

}
